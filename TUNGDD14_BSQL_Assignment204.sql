﻿--Câu 1
---Tạo bảng Movie ---
CREATE TABLE Movie(
	name nvarchar(255) NOT NULL PRIMARY KEY, -- Tên phim--
	duration int CHECK(duration>3600) NOT NULL, --Thời lượng phim tính bằng giây (s)
	genre tinyint CHECK(genre BETWEEN 1 AND 8) NOT NULL, ---Thể loại:  1: Hành động, 2: Phiêu lưu, 3: Hài, 4: Tội phạm (xã hội đen), 5: Phim truyền hình, 6: Kinh dị, 7: Ca nhạc / khiêu vũ, 8: Chiến tranh
	director nvarchar(255) NOT NULL, --- Đạo diễn---
	amount decimal(10,2) NOT NULL, ---Số tiền kiếm được---
	comments text ---Nhận xét---
	)

---Tạo bảng Actor ---
CREATE TABLE Actor (
	name nvarchar(255)  NOT NULL PRIMARY KEY  ,---Tên Diên viên--
	age tinyint NOT NULL ,--- Tuổi Diên viên---
	averageSaraly decimal(10,2) NOT NULL,--- Mức lương trung bình---
	nationality nvarchar(20) NOT NULL, ---Quốc tịch---
	)

---Tạo bảng ActedIn ---
CREATE TABLE ActedIn (
	id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	name_movie nvarchar(255) NOT NULL ,
	FOREIGN KEY (name_movie) REFERENCES Movie(name) ON UPDATE CASCADE,
	name_actor nvarchar(255) NOT NULL ,
	FOREIGN KEY (name_actor) REFERENCES Actor(name) ON UPDATE CASCADE,
	)

---Thêm cột ImageLink vào bảng Movie---
ALTER TABLE Movie ADD ImageLink varchar(255) UNIQUE;


---Câu 2---
---1---
---Thêm các bản ghi vào bảng Movie---
INSERT INTO Movie(name,duration,genre,director,amount,comments,ImageLink) 
VALUES (N'Hành động',7200,1,N'Duy Tung',100000,N'Phim chat lam','http://1'),
		(N'Phiêu lưu',8500,4,N'Minh Dat',50000,N'Phim chan qua','http://2'),
		(N'Hài',7200,7,'Chi Trung',70000,N'Phim xem dc','http://3'),
		(N'Tội phạm (xã hội đen)',15000,8,N'Duy Tung',150000,null,'http://4'),
		(N'Phim truyền hình',20000,5,N'Quang teo',30000,null,'http://5'),
		(N'Kinh dị',15000,8,N'Thùy Linh',150000,null,'http://6'),
		(N'Ca nhạc / khiêu vũ',15000,8,N'Tiến Minh',150000,null,'http://7'),
		(N'Chiến tranh',15000,8,N'Minh Uyên',150000,null,'http://8')

---Thêm các bản ghi vào bảng Actor---
INSERT INTO Actor(name, age,averageSaraly,nationality)
VALUES(N'Actor1', 18,800,N'Mỹ'),
(N'Actor2', 60,800,N'Mỹ'),
(N'Actor3', 19,100,N'Anh'),
(N'Actor4', 28,1200,N'Hàn Quốc'),
(N'Actor5', 56,1400,N'Thái Lan'),
(N'Actor6', 08,1600,N'Việt Nam')


---Thêm các bản ghi vào bảng ActedIn---

INSERT INTO ActedIn(name_movie,name_actor) 
VALUES(N'Hành động',N'Actor1'),
(N'Hành động',N'Actor2'),
(N'Hành động',N'Actor3'),
(N'Hành động',N'Actor4'),
(N'Hành động',N'Actor5'),

(N'Phiêu lưu',N'Actor1'),
(N'Phiêu lưu',N'Actor2'),
(N'Phiêu lưu',N'Actor3'),
(N'Phiêu lưu',N'Actor4'),

(N'Hài',N'Actor2'),
(N'Hài',N'Actor3'),
(N'Hài',N'Actor4'),
(N'Chiến tranh',N'Actor5'),

(N'Chiến tranh',N'Actor3'),
(N'Chiến tranh',N'Actor4'),
(N'Phim truyền hình',N'Actor5'),
(N'Phim truyền hình',N'Actor6')


---2---

UPDATE Actor 
SET name='NGUOI THAY THE'
WHERE name='Actor2';



----Câu 3: ----
---1 Hiển thị các Diên viên trên 50 tuổi---
SELECT * FROM Actor WHERE age > 50 

--- 2 Hiển thị tên và mức lương và sắp xếp theo mức lương mặc định tăng dần
SELECT name, averageSaraly
FROM Actor
ORDER BY averageSaraly   --- DESC giảm dần  ASC tăng dần ---  

---3 Hiển thị tên các phim mà diên viên Actor2 đã đóng
SELECT name
FROM Movie,(SELECT * 
FROM ActedIn
WHERE name_actor = 'Actor2')AS TEMP
WHERE Movie.name = TEMP.name_movie


--- 4 Hiển thị tên phim mà có >3 diễn viên đóng 

SELECT name
FROM Movie,(SELECT name_movie
FROM ActedIn
GROUP BY name_movie ---nhóm các bản ghi ---
HAVING Count(name_actor)>3) AS TEMP ---lọc các bản ghi có số lượng lớn hơn 3
WHERE Movie.name = TEMP.name_movie


