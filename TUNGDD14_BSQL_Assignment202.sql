﻿
---cau 1---
---tạo bảng TRAINEE ----
CREATE TABLE TRAINEE (
			TraineeID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
			Full_Name varchar(100),
			Birth_Date date,
			Gender  varchar(15) CHECK(Gender IN ('male','female')), ---Giớ tính cso hai giá trị "male" vào "female"---
			ET_IQ int CHECK(ET_IQ BETWEEN 0 AND 20), ---Điểm ET_IQ từ 0-20 ---
			ET_Gmath int CHECK(ET_Gmath BETWEEN 0 AND 20),---Điểm ET_Gmath từ 0-20 ---
			ET_English int CHECK(ET_English BETWEEN 0 AND 50) ,---Điểm ET_English từ 0-50 ---
			Training_Class varchar(255),
			Evaluation_Notes varchar(255),
			 );

--- Thêm bản ghi vào bảng TRAINEE ---
INSERT INTO TRAINEE(Full_Name,Birth_Date,Gender,ET_IQ,ET_Gmath,ET_English,Training_Class,Evaluation_Notes) 
VALUES('Nguyen Van A','1993-05-01', 'male',10,11,20,'HN20_CPL_.NET','no'),
		('Luu Manh Thai','1994-08-01', 'male',9,12,10,'HN20_CPL_.NET','no'),
		('Ha Manh Trung','1998-02-01', 'male',19,19,45,'HN20_CPL_.NET','pass'),
		('Pham Thanh Dat','1998-04-01', 'male',18,15,35,'HN20_CPL_.NET','no'),
		('Mai Hong Anh','1997-11-30', 'female',17,15,40,'HN20_CPL_.NET','pass'),
		('Bui Thu Uyen','1999-11-28', 'female',15,19,40,'HN20_CPL_.NET','pass'),
		('Pham Van Hung','2000-06-28', 'male',10,17,41,'HN20_CPL_.NET','no'),
		('Ong Thi Linh','2001-01-02', 'female',14,19,47,'HN20_CPL_.NET','no'),
		('Chu Thi Huyen Trang','2000-06-27', 'female',16,18,37,'HN20_CPL_.NET','pass'),
		('Lo Chi Ton','1995-10-10', 'male',14,18,39,'HN20_CPL_.NET','pass')

---cau 2 ---

ALTER TABLE TRAINEE ADD Fsoft_Account varchar(255) NOT NULL UNIQUE 

---cau3 Tạo bảng View ---
CREATE VIEW PASS
AS
SELECT Birth_Date 
FROM TRAINEE 
WHERE (ET_IQ + ET_Gmath) >=20 AND ET_IQ>=8 AND ET_Gmath>=8  AND ET_English>=18​ 


--- Cau 4 ---
SELECT MONTH(Birth_Date) AS month, COUNT( TraineeID) AS count
FROM trainee 
WHERE ET_IQ + ET_Gmath >=20 and ET_IQ>=8 and ET_Gmath>=8 and ET_English>=18​ ---Điên kiện qua ET---
GROUP BY MONTH(Birth_Date)

//cau 5
SELECT *, DATEDIFF(year,Birth_Date, '2020') AS age ---DATEDIFF hàm tính tuổi---
FROM TRAINEE
WHERE LEN(Full_Name) = (SELECT MAX(stringLength) as nam ---Hiện giá trị lớn nhất---
FROM TRAINEE,(SELECT LEN(Full_Name) AS stringLength FROM TRAINEE) AS length) ---Hiển thị dộ dài các chuỗi(trường Fullname) của bản ghi---



